// ==UserScript==
// @name        Disable Advanced Investment Buttons
// @namespace   http://userscripts.xcom-alliance.info/
// @description Disable the skills trade-in buttons on the Advanced Skill Management screen
// @author      Miche (Orion) / Sparkle (Artemis)
// @include     http*://*.pardus.at/overview_advanced_skills.php*
// @version     1.0
// @grant       none
// @updateURL 	http://userscripts.xcom-alliance.info/disable_investment_buttons/disable_advanced_investment_buttons.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/disable_investment_buttons/disable_advanced_investment_buttons.user.js
// @icon 		http://userscripts.xcom-alliance.info/disable_investment_buttons/icon.png
// ==/UserScript==

var nodes = document.querySelectorAll('input[value="Invest into ASP Pool"]');
for (var loop=0; loop<nodes.length; loop++) {
    nodes[loop].disabled = true;
    nodes[loop].className = 'disabled';
}